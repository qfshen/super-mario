package su;

import java.awt.image.BufferedImage;

public class Obstacle implements Runnable {
    //用于表示坐标
    private int x;
    private int y;
    //用于记录障碍物类型
    private int type;
    //用于显示图像
    private BufferedImage show = null;
    //定义当前的场景对象
    private  BackGround bg = null;
    //定义一个线程的对象
    Thread thread = new Thread(this);

    public Obstacle(int x, int y, int type,  BackGround bg) {
        //坐标
        this.x = x;
        this.y = y;
        //用于记录障碍物类型
        this.type = type;
        //定义当前的场景对象
        this.bg = bg;
        //得到集合中StaticValue类下obstacle集合的第type个障碍物
        show = StaticValue.obstacle.get(type);
        //如果是旗子的话 启动线程
        if (type==8){
            thread.start();
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getType() {
        return type;
    }

    public BufferedImage getShow() {
        return show;
    }

    @Override
    public void run() {
        while (true){
            if (this.bg.isReach()){
                if (this.y<374){
                    this.y +=5;
                }else {
                    //表示旗子已经落到了地上
                    this.bg.setBase(true);
                }
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
