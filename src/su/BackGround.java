package su;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
//背景类
public class BackGround {
    //显示当前场景的图片
    private BufferedImage bgImage = null;
    //记录当前第几个场景
    private int sort;
    //判断是否为最后一个场景
    private boolean flag;
    //用于显示旗杆
    private BufferedImage gan = null;
    //用于显示城堡
    private BufferedImage tower = null;
    //判断马里奥是否到达旗杆位置
    private boolean isReach = false;
    //判断马里奥是否落地
    private boolean isBase = false;
    //用于存放所有的敌人
    private List<Enemy> enemyList = new ArrayList<>();
    //用于存放我们所有的障碍物
    private List<Obstacle> obstacleList = new ArrayList<>();

    private static BufferedImage start = null;


    public  BackGround() {

    }

    //负责调用绘制的方法
    public BackGround(int sort, boolean flag) {
        this.sort = sort;
        this.flag = flag;
        start = StaticValue.start;
        //判断是否为最后一个场景  如果是则将图片切换为最后一个场景
        if (flag) {
            bgImage = StaticValue.bg2;
        } else {
            bgImage = StaticValue.bg;
        }
        //绘制第一关的障碍物
        if (sort == 1) {
            //地面
            //type是StaticValue类的障碍物
            //绘制第一关的地面  上地面type=1  下地面type=2
            for (int i = 0; i < 27; i++) {
                obstacleList.add(new Obstacle(i * 30, 420, 1, this));
            }
            //绘制砖块
            for (int j = 0; j <= 120; j += 30) {
                for (int i = 0; i < 27; i++) {
                    obstacleList.add(new Obstacle(i * 30, 570 - j, 2, this));
                }
            }
            //绘制砖块A
            for (int i = 120; i <= 150; i += 30) {
                obstacleList.add(new Obstacle(i, 300, 7, this));
            }
            //绘制砖块B-F
            for (int i = 300; i <= 570; i += 30) {
                if (i == 360 || i == 390 || i == 480 || i == 510 || i == 540) {
                    obstacleList.add(new Obstacle(i, 300, 7, this));
                } else {
                    obstacleList.add(new Obstacle(i, 300, 0, this));
                }
            }
            //砖块G
            for (int i = 420; i <= 450; i += 30) {
                obstacleList.add(new Obstacle(i, 240, 7, this));
            }
            //水管
            for (int i = 360; i <= 600; i += 25) {
                if (i == 360) {
                    obstacleList.add(new Obstacle(620, i, 3, this));
                    obstacleList.add(new Obstacle(645, i, 4, this));
                } else {
                    obstacleList.add(new Obstacle(620, i, 5, this));
                    obstacleList.add(new Obstacle(645, i, 6, this));
                }
            }
            //蘑菇
            enemyList.add(new Enemy(580, 385, true, this, 1));
            //食人花
            enemyList.add(new Enemy(635, 420, true, 2, 328, 418, this));
        }
        //绘制第二关的障碍物
        if (sort == 2) {
            //地面
            for (int i = 0; i < 27; i++) {
                obstacleList.add(new Obstacle(i * 30, 420, 1, this));
            }
            for (int j = 0; j <= 120; j += 30) {
                for (int i = 0; i < 27; i++) {
                    obstacleList.add(new Obstacle(i * 30, 570 - j, 2, this));
                }
            }
            //第一个水管
            for (int i = 360; i <= 600; i += 25) {
                if (i == 360) {
                    obstacleList.add(new Obstacle(60, i, 3, this));
                    obstacleList.add(new Obstacle(85, i, 4, this));
                } else {
                    obstacleList.add(new Obstacle(60, i, 5, this));
                    obstacleList.add(new Obstacle(85, i, 6, this));
                }
            }
            //第二个水管
            for (int i = 330; i <= 600; i += 25) {
                if (i == 330) {
                    obstacleList.add(new Obstacle(620, i, 3, this));
                    obstacleList.add(new Obstacle(645, i, 4, this));
                } else {
                    obstacleList.add(new Obstacle(620, i, 5, this));
                    obstacleList.add(new Obstacle(645, i, 6, this));
                }
            }
            //砖块C
            obstacleList.add(new Obstacle(300, 330, 0, this));
            //砖块B,E,G
            for (int i = 270; i <= 330; i += 30) {
                if (i == 270 || i == 330) {
                    obstacleList.add(new Obstacle(i, 360, 0, this));
                } else {
                    obstacleList.add(new Obstacle(i, 360, 7, this));
                }
            }
            //砖块A,D,F,H,I
            for (int i = 240; i <= 360; i += 30) {
                if (i == 240 || i == 360) {
                    obstacleList.add(new Obstacle(i, 390, 0, this));
                } else {
                    obstacleList.add(new Obstacle(i, 390, 7, this));
                }
            }
            //妨碍砖块
            obstacleList.add(new Obstacle(240, 300, 0, this));
            for (int i = 360; i <= 540; i += 60) {
                obstacleList.add(new Obstacle(i, 270, 7, this));
            }
            //食人花敌人
            enemyList.add(new Enemy(75, 420, true, 2, 325, 418, this));
            enemyList.add(new Enemy(635, 420, true, 2, 298, 358, this));
            //蘑菇敌人
            enemyList.add(new Enemy(200, 385, true, this, 1));
            enemyList.add(new Enemy(500, 385, true, this, 1));
        }
        //绘制第三关的障碍物
        if (sort == 3) {
            //地面
            for (int i = 0; i < 27; i++) {
                obstacleList.add(new Obstacle(i * 30, 420, 1, this));
            }
            for (int j = 0; j <= 120; j += 30) {
                for (int i = 0; i < 27; i++) {
                    obstacleList.add(new Obstacle(i * 30, 570 - j, 2, this));
                }
            }
            //A-O砖块
            int temp = 290;
            for (int i = 390; i >= 270; i -= 30) {
                for (int j = temp; j <= 410; j += 30) {
                    obstacleList.add(new Obstacle(j, i, 7, this));
                }
                temp += 30;
            }
            //P-R砖块
            temp = 60;
            for (int i = 390; i >= 360; i -= 30) {
                for (int j = temp; j <= 90; j += 30) {
                    obstacleList.add(new Obstacle(j, i, 7, this));
                }
                temp += 30;
            }
            //旗杆
            gan = StaticValue.gan;
            //城堡
            tower = StaticValue.tower;
            //棋子加到旗杆上
            obstacleList.add(new Obstacle(515, 220, 8, this));
            enemyList.add(new Enemy(150, 385, true, this, 1));
        }
        
    }

    //获取旗杆图片
    public BufferedImage getGan() {
        return gan;
    }
    //获取未开始图片
    public static BufferedImage getStart() {
        return start;
    }

    //获取城堡图片
    public BufferedImage getTower() {
        return tower;
    }
    //将背景图片返回的方法
    public BufferedImage getBgImage() {
        return bgImage;
    }

    public int getSort() {
        return sort;
    }

    public boolean isFlag() {
        return flag;
    }
    //将obstacleList集合返回的方法
    public List<Obstacle> getObstacleList() {
        return obstacleList;
    }

    public List<Enemy> getEnemyList() {
        return enemyList;
    }

    public boolean isBase() {
        return isBase;
    }

    public void setBase(boolean base) {
        isBase = base;
    }

    public boolean isReach() {
        return isReach;
    }

    public void setReach(boolean reach) {
        isReach = reach;
    }

}
